'use strict';

const POUNDS_TO_EUROS = 1.12;
const DOLLARS_TO_EUROS = 0.81;
const LINKEDIN_HOUR_OFFSET_CORRECTION = +9;

const LanguageDetect = require('languagedetect');

var lngDetector = new LanguageDetect();


function calculateLength(message) {
  message.messageLength = message.content.length;

  return message;
}

function dayOfWeek(message) {
  var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  message.dayOfWeek = days[message.date.getDay()];

  return message;
}

function hourOfDay(message) {
  message.hourOfDay = message.date.getHours();

  return message;
}

function min(salary) {
  var min = salary[0];

  salary.forEach(s => {
    if (s<min) min = s;
  });

  return min;
}

function max(salary) {
  var max = salary[0];

  salary.forEach(s => {
    if (s>max) max = s;
  });

  return max;
}

function addSalary(message) {
  var matches = message.content.match(/([:( €£$]+[1-9][0-9]*?[\.\,]?0{3}[€£$ ]?)/g);
  var matchesRange = message.content.match(/([:( €£$]?\d+?[\-/\s]?[\d.,]+\s*(k|K|eur|EUR))/g)
  if (matches || matchesRange) {
    if (!matches) matches = [];
    if (!matchesRange) matchesRange = [];

    message.salary = matches.concat(matchesRange).join(', ');

    var pounds = matches.concat(matchesRange).join(',').indexOf('£') != -1;
    var dollars = matches.concat(matchesRange).join(',').indexOf('$') != -1;

    var salary = matches.concat(matchesRange).map(match => {
      var parsed = match.trim().replace(/[(:\,\.\s]/g, '').replace(/k/, '000').replace(/K/, '000');

      //en el caso de un match tipo 25-30k nos quedamos con el 30k
      //ya, no es lo más preciso, pero estamos dentro de un map()
      //convertir este elemento en dos (el 25 y el 30) no es trivial. para la v2
      parsed = parsed.replace(/\d\d[\-\s/]+/, '');

      if (parsed.indexOf('€') != -1) {
        parsed = parseInt(parsed.replace('€', ''), 10);
      } else if (parsed.indexOf('EUR') != -1) {
        parsed = parseInt(parsed.replace('EUR', ''), 10);
      } else if (parsed.indexOf('£') != -1) {
        parsed = parseInt(parsed.replace('£', ''), 10);
        parsed = parsed * POUNDS_TO_EUROS;
      } else if (parsed.indexOf('$') != -1) {
        parsed = parseInt(parsed.replace('$', ''), 10);
        parsed = parsed * DOLLARS_TO_EUROS;
      } else {
        parsed = parseInt(parsed, 10);
        if (pounds) parsed = parsed * POUNDS_TO_EUROS;
        else if (dollars) parsed = parsed * DOLLARS_TO_EUROS;
      }

      return parsed;
    });

    salary = salary.filter(s => { return s > 10000; });

    if (salary.length > 0) {
      message.minSalary = Math.round(min(salary));
      message.maxSalary = Math.round(max(salary));
    }
  }
  return message;
}

function englishOrSpanish(languages) {
  var english = 0;
  var spanish = 0;
  languages.forEach(lang => {
    if (lang[0] == 'english') {
      english = lang[1];
    } else if (lang[0] == 'spanish') {
      spanish = lang[1];
    }
  });

  return (english > spanish) ? 'english' : 'spanish';
}

function detectLanguage(message) {
  var languages = lngDetector.detect(message.content);
  message.language = englishOrSpanish(languages);
  return message;
}

function parseDate(dateStr) {
  // format: 2022-10-11 14:04:34 UTC
  var dateParts = dateStr.split(' ');
  var d = new Date(dateParts[0]);

  var hourParts = dateParts[1].split(':');
  d.setHours(parseInt(hourParts[0], 10));
  d.setMinutes(parseInt(hourParts[1], 10));
  d.setSeconds(0);
  d.setMilliseconds(0);

  return d;
}

function parseMessage(data) {
  var message = {
    'from': data.from,
    'to': data.to,
    'date': parseDate(data.date),
    'subject': data.subject,
    'content': data.content,
    'direction': data.from=='Salva Labajos Samos' ? 'OUTGOING' : 'INCOMING'
  };

  addExtraInfo(message);

  return message;
}

function addExtraInfo(message) {
  message = dayOfWeek(message);
  message = hourOfDay(message);
  message = detectLanguage(message);
  if (message.language == 'english') {
    message.content_en = message.content;
  }
  if (message.language == 'spanish') {
    message.content_es = message.content;
  }
  message = calculateLength(message);

  if (message.direction == 'INCOMING') {
    message = addSalary(message);
  }

  return message;
}

function parse(record) {
  try {
    let rawData = {
      'from': record[2],
      'to': record[4],
      'date': record[5],
      'subject': record[6],
      'content': record[7]
    };
    return parseMessage(rawData);
  } catch (err) {
    console.error('Failed to parse row: ' + record.join(','));
    console.error(err.message);
    return;
  }
}

module.exports = {
  parse: parse,
  parseMessage: parseMessage
};
