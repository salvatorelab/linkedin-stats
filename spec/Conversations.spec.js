'use strict';

const Conversations = require('../Conversations');

describe("Conversations", function() {
  it("should return an incoming message without an answer", function() {
    var conversations = new Conversations();

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Bill Gates',
      'to': 'Salva',
      'date': new Date('2018-03-18T00:00:00')
    });

    var messages = conversations.getConversationWith('Bill Gates');

    expect(messages.length).toBe(1);
    expect(messages[0].answered).toBe(false);
  });

  it("should return an incoming that was answered", function() {
    var conversations = new Conversations();

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Bill Gates',
      'to': 'Salva',
      'content': 'Wanna work for Microsoft?',
      'date': new Date('2018-03-18T00:00:00')
    });

    conversations.addMessage({
      'direction': 'OUTGOING',
      'from': 'Salva',
      'to': 'Bill Gates',
      'content': 'Nah thanks :)',
      'date': new Date('2018-03-18T11:11:11')
    });

    var messages = conversations.getConversationWith('Bill Gates');

    expect(messages.length).toBe(2);
    expect(messages[0].answered).toBe(true);
  });

  it("should return two incoming messages that got no answer", function() {
    var conversations = new Conversations();

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Bill Gates',
      'to': 'Salva',
      'content': 'Wanna work for Microsoft?',
      'date': new Date('2018-03-18T00:00:00')
    });

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Bill Gates',
      'to': 'Salva',
      'content': 'We love open source!',
      'date': new Date('2018-03-18T00:33:00')
    });

    var messages = conversations.getConversationWith('Bill Gates');

    expect(messages.length).toBe(2);
    expect(messages[0].answered).toBe(false);
    expect(messages[1].answered).toBe(false);
  });

  it("should return two incoming messages, the second one got an answer", function() {
    var conversations = new Conversations();

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Bill Gates',
      'to': 'Salva',
      'content': 'Wanna work for Microsoft?',
      'date': new Date('2018-03-18T00:00:00')
    });

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Bill Gates',
      'to': 'Salva',
      'content': 'We love open source!',
      'date': new Date('2018-03-18T00:33:00')
    });

    conversations.addMessage({
      'direction': 'OUTGOING',
      'from': 'Salva',
      'to': 'Bill Gates',
      'content': 'Nah thanks :)',
      'date': new Date('2018-03-18T11:11:11')
    });

    var messages = conversations.getConversationWith('Bill Gates');

    expect(messages.length).toBe(3);
    expect(messages[0].answered).toBe(false);
    expect(messages[1].answered).toBe(true);
  });

  it("should return two messages on a conversation that was ignored", function() {
    var conversations = new Conversations();

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Kim Jong-un',
      'to': 'Salva',
      'content': 'Hey, wanna build some missile guidance software?',
      'date': new Date('2018-08-18T00:00:00')
    });

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Kim Jong-un',
      'to': 'Salva',
      'content': 'How about drones? Do you like drones?',
      'date': new Date('2018-08-19T00:00:00')
    });

    var messages = conversations.getConversationWith('Kim Jong-un');

    expect(messages.length).toBe(2);
    expect(messages[0].conversationIgnored).toBe(true);
    expect(messages[1].conversationIgnored).toBe(true);
  });

  it("should return three messages on a conversation that was NOT ignored", function() {
    var conversations = new Conversations();

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Trinity',
      'to': 'Neo',
      'content': 'Wake up, Neo...',
      'date': new Date('2199-01-01T00:00:00')
    });

    conversations.addMessage({
      'direction': 'OUTGOING',
      'from': 'Neo',
      'to': 'Trinity',
      'content': 'What?',
      'date': new Date('2199-01-01T00:00:02')
    });

    conversations.addMessage({
      'direction': 'INCOMING',
      'from': 'Trinity',
      'to': 'Neo',
      'content': 'The Matrix has you...',
      'date': new Date('2199-01-01T00:00:03')
    });

    var messages = conversations.getConversationWith('Trinity');

    expect(messages.length).toBe(3);
    expect(messages[0].conversationIgnored).toBe(false);
    expect(messages[1].conversationIgnored).toBe(false);
    expect(messages[2].conversationIgnored).toBe(false);
    expect(messages[2].answered).toBe(false); //conversation NOT ignored, but message unanswered
  });
});
