'use strict';

const parser = require('../parser');

const ME = 'Salva Labajos Samos';

describe("Messages parser", function() {
  it("should parse date", function() {
    var data = {
      from: 'from',
      to: 'to',
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'A message content'
    };

    var message = parser.parseMessage(data);

    expect(message.date.getTime()).toBe(new Date('2018-02-22T14:19:00').getTime());
  });

  it("should fill minSalary", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'A message content with €40k and also 45.000 too!'
    };

    var message = parser.parseMessage(data);

    expect(message.minSalary).toBe(40000);
  });

  it("should parse salary starting with space", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'The salary would be: 45000'
    };

    var message = parser.parseMessage(data);

    expect(message.minSalary).toBe(45000);
  });

  it("should parse salary starting with euro symbol", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'The salary would be: €45000'
    };

    var message = parser.parseMessage(data);

    expect(message.minSalary).toBe(45000);
  });

  it("should parse salary ending with euro symbol", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'The salary would be: 45.000€'
    };

    var message = parser.parseMessage(data);

    expect(message.minSalary).toBe(45000);
  });

  it("should not parse number as salary", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'You can contact me directly on 016719000 or mail me rick@pawnshop.com'
    };

    var message = parser.parseMessage(data);

    expect(message.salary).not.toBeDefined();
  });

  it("should fill maxSalary", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'A message content with €40k and also 45.000 too!'
    };

    var message = parser.parseMessage(data);

    expect(message.maxSalary).toBe(45000);
  });

  it("should fill min and max salary when in range like XX-YY", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'A message content with 40-45k'
    };

    var message = parser.parseMessage(data);

    expect(message.minSalary).toBe(45000); // try to improve this in the future
    expect(message.maxSalary).toBe(45000);
  });

  it("should parse salary range ending with EUR as currency and comas instead of dots", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'Salary: 72-117,000 EUR gross/year'
    };

    var message = parser.parseMessage(data);

    expect(message.maxSalary).toBe(117000);
  });

  it("should parse min and max salary", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'Sueldo: cifra meramente orientativa (35.000€- 45.0000 brutos/año)'
    };

    var message = parser.parseMessage(data);

    expect(message.minSalary).toBe(35000);
    expect(message.maxSalary).toBe(45000);
  });

  it("should parse range with slash symbol", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'Salario 25/30k'
    };

    var message = parser.parseMessage(data);

    expect(message.maxSalary).toBe(30000);
  });

  it("should parse salary range with hyphen and k in the end", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'Contato indefinido (60-65k)'
    };

    var message = parser.parseMessage(data);

    expect(message.maxSalary).toBe(65000);
  });

  it("should parse salary range with hyphen and k in both ends", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'El salario de entrada está entre 24k-30k aproximadamente'
    };

    var message = parser.parseMessage(data);

    expect(message.maxSalary).toBe(30000);
  });

  it("should parse salary with > symbol before", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 14:19:00 UTC',
      subject: 'A subject',
      content: 'for the right person (some >100k)'
    };

    var message = parser.parseMessage(data);

    expect(message.maxSalary).toBe(100000);
  });

  it("should fill hour of day (AM)", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 10:46:00 UTC',
      subject: 'A subject',
      content: 'A message content'
    };

    var message = parser.parseMessage(data);

    expect(message.hourOfDay).toBe(10);
  });

  it("should fill hour of day (PM)", function() {
    var data = {
      from: 'from',
      to: ME,
      date: '2018-02-22 22:46:00 UTC',
      subject: 'A subject',
      content: 'A message content'
    };

    var message = parser.parseMessage(data);

    expect(message.hourOfDay).toBe(22);
  });
});
