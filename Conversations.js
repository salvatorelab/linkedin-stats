'use strict';

var Conversation = function() {
  this.messagesByUser = {};
};

Conversation.prototype.addMessage = function(message) {
  var conversationWith = (message.direction === 'INCOMING') ? message.from : message.to;

  var messages = this.messagesByUser[conversationWith];
  if (!messages) {
    messages = [message];
  } else {
    messages.push(message);
    messages = messages.sort(function(a, b) {
      return a.date.getTime() - b.date.getTime();
    });
  }

  this.messagesByUser[conversationWith] = messages;
}

Conversation.prototype.getConversationWith = function(user) {
  var messages = this.messagesByUser[user];
  var conversationIgnored = allMessagesFromSameAuthorAndDirection(messages);

  for (var i=0; i<messages.length; i++) {
    var message = messages[i];
    message.conversationIgnored = conversationIgnored;
    message.isLead = isLead(message, messages);
    if (message.direction === 'INCOMING') {
      if (i !== messages.length-1) {
        message.answered = messages[i + 1].direction !== 'INCOMING';
      } else {
        message.answered = false;
      }
    }
  }

  return messages;
}

Conversation.prototype.getAllMessages = function() {
  var _this = this;

  var messages = [];

  Object.keys(this.messagesByUser).forEach(function (user) {
    messages = messages.concat(_this.getConversationWith(user));
  });

  return messages;
}

function allMessagesFromSameAuthorAndDirection(messages) {
  var direction = messages[0].direction;
  var from = messages[0].from;
  var to = messages[0].to;

  for (var i=0; i<messages.length; i++) {
    var message = messages[i];
    if (message.direction !== direction || message.from !== from || message.to !== to) {
      return false;
    }
  }

  return true;
}

function isLead(message, messages) {
  let previousMessage = null;
  for (var i=0; i<messages.length; i++) {
    if (messages[i].content == message.content) {
      if (!previousMessage) return true;

      let monthBeforeThisMessage = new Date(messages[i].date);
      monthBeforeThisMessage.setDate(monthBeforeThisMessage.getDate() - 30);
      if (previousMessage.date < monthBeforeThisMessage) return true;
      else return false;
    } else {
      previousMessage = messages[i];
    }
  }
  return false;
}

module.exports = Conversation;
