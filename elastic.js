'use strict';

const { Client } = require('@elastic/elasticsearch');
const INDEX_NAME = 'linkedin';

const client = new Client({
  node: 'http://localhost:9200'
});

async function configureIndex() {
  await client.indices.create({
    index: INDEX_NAME,
    settings: {

    },
    mappings: {
      properties: {
        "content": {
          "type": "text",
          "fielddata": true,
          "analyzer": "english"
        },
        "content_en": {
          "type": "text",
          "fielddata": true,
          "analyzer": "english"
        },
        "content_es": {
          "type": "text",
          "fielddata": true,
          "analyzer": "spanish"
        }
      }
    }
  });
}

async function insertMessage(message) {
  return client.index({
    index: INDEX_NAME,
    document: message
  });
}

module.exports = {
  configureIndex: configureIndex,
  insertMessage: insertMessage
};
